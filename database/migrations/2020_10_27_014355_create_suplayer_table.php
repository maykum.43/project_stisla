<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuplayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suplayer', function (Blueprint $table) {
            $table->string('id_suplayer',10)->primary();
            $table->string('nama_suplayer',100);
            $table->string('no_telp',13);
            $table->string('alamat',100);
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suplayer');
    }
}
