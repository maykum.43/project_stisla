<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'name'   => 'Fikri',
                'email'  => '1234abc@email.com',
                'password' =>  bcrypt('12345678'),
                'created_at'    => now(),
            ),
            array(
                'name'   => 'Maulana',
                'email'  => '5678def@email.com',
                'password' =>  bcrypt('123456'),
                'created_at'    => now(),
            ),
        ));
    }
}
