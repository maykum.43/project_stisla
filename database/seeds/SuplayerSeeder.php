<?php

use Illuminate\Database\Seeder;

class SuplayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suplayer')->insert(array(
            array(
                'id_suplayer'   => 'Sup-001',
                'nama_suplayer' => 'PT. Tirta',
                'no_telp'       => '02100013',
                'alamat'        => 'Bogor',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
            array(
                'id_suplayer'   => 'Sup-002',
                'nama_suplayer' => 'PT. Sukses Bersama',
                'no_telp'       => '022086989',
                'alamat'        => 'Padalarang',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
