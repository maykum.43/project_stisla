<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = 'barang';
    protected $fillable = [
        'nama_barang','harga_satuan','stok','keterangan','is_active','id_suplayer'
    ];

    public function haveSuplayer(){
        return $this->belongTo(SuplayerModel::class,'id_suplayer','id_suplayer');
    }
}
