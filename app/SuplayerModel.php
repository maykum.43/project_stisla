<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuplayerModel extends Model
{
    protected $table = 'suplayer';
    public function hasManyProduct(){
        return $this->hasMany(BarangModel::class,'id_suplayer','id_suplayer');
    }
}
